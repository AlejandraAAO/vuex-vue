Vue.component('child',{
    template:`
    <div class="py-5 bg-warning">
    <!--llamamos al nombre del prop-->
        <h4>Componente hijo: {{numero}}</h4>
        <!--info local en data-->    
        <h4>Nombre: {{name}}</h4>
        <button @click="numero++">+</button>
    </div>
    `,
    //el hijo a travez de props va a recibir la info del comp padre
    props: [ 
        //el nombre del prop es el nombre q nosotros le colocamos en el padre
      'numero'
    ],
    data(){
        return{
            name:'nugget'

        }
    },
    //ejecuta cuando el dom ya esta listo, cuando todoo este pintado
    mounted(){
        //emitir un evento
        //         (pasando un nombre, el dato)
        this.$emit('nameHijo',this.name);
    }
})