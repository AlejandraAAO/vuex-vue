//componentes pueden tener otros componentes dentro
Vue.component('father',{
    template:`
    <div class="p-5 bg-dark text-white">
        <h2>Componente padre: {{numeroFather}}</h2>
        
        <button class="btn btn-danger" @click='numeroFather++'>+</button>
        <!--mandamos el numero 5 a nuestro componente hijo
        la info la pasamo como normalmente se pasa, pero le añadimos el : del v-bind
        recibimos el evento del hijo con el @, lo igualamos al data , q es igual al evento-->
        
        {{nameFather}}
        <child :numero="numeroFather" @nameHijo="nameFather=$event"></child>
    </div>`,
    data(){
        return{
            numeroFather:0,
            //tenemos como un string q se sobreescribira con la info 
            //q venga del hijo
            nameFather:''

        }
    }
})